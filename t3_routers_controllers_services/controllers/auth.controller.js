const statusCodes = require('../configs/statusCodes.enam');
const {
  authService,
} = require('../services');

module.exports = {
  login: (req, res) => authService.loginUser(req.body)
    .then((data) => {
      if (data.isOk) {
        res.json(data);
        return;
      }
      res.status(data.status).json(data);
    })
    .catch((err) => {
      res.status(statusCodes.BAD_REQUEST).json({ message: err.message || 'Error' });
    }),
};
