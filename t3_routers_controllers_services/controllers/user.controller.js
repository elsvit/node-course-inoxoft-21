const statusCodes = require('../configs/statusCodes.enam');
const {
  userService,
} = require('../services');

module.exports = {
  createUser: (req, res) => userService.saveUser(req.body)
    .then((data) => {
      if (data.isOk) {
        res.json(data);
        return;
      }
      res.status(data.status).json(data);
    })
    .catch((err) => {
      console.log('saveUser ERROR');
      res.status(statusCodes.BAD_REQUEST).json({ message: err.message || 'Error' });
    }),

  getUserByName: (req, res) => {
    const { user_name } = req.params;
    return userService.getUser(user_name)
      .then((data) => {
        if (data.isOk) {
          res.json(data);
          return;
        }
        res.status(data.status).json(data);
      })
      .catch((err) => {
        console.log('getUserByName ERROR');
        res.status(statusCodes.BAD_REQUEST).json({ message: err.message || 'Error' });
      });
  },

  getAllUsers: (req, res) => userService.getUsers()
    .then((data) => {
      if (data.isOk) {
        res.json(data);
        return;
      }
      res.status(data.status).json(data);
    })
    .catch((err) => {
      console.log('getAllUsers ERROR');
      res.status(statusCodes.BAD_REQUEST).json({ message: err.message || 'Error' });
    }),
};
