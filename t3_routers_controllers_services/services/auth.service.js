const fs = require('fs');
const path = require('path');
const util = require('util');

const statusCodes = require('../configs/statusCodes.enam');

const fsReadPromise = util.promisify(fs.readFile);

const usersPath = path.join(__dirname, '..', 'dataBase', 'users.json');

const loginUser = (askedUser) => fsReadPromise(usersPath)
  .then((res) => {
    const users = JSON.parse(res.toString());
    const user = users.find((val) => (val.name === askedUser.name));

    if (!user) {
      return ({ isOk: false, status: statusCodes.NOT_FOUND, message: 'User not found' });
    } if (user.password !== askedUser.password) {
      return ({ isOk: false, status: statusCodes.CONFLICT, message: 'Password is wrong' });
    }

    return ({ isOk: true, data: user });
  })
  .catch((err) => ({ isOk: false, status: statusCodes.BAD_REQUEST, message: err.message || 'Error login User' }));

module.exports = {
  loginUser,
};
