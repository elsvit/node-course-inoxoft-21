const router = require('express').Router();
const { userController } = require('../controllers');

router.post('/', userController.createUser);

router.get('/', userController.getAllUsers);

router.get('/:user_name', userController.getUserByName);

// router.delete('/:user_id', userController.deleteUserById);

module.exports = router;
