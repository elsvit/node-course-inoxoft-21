const { userService } = require('../services');

module.exports = {
  createUser: async (req, res, next) => {
    try {
      const user = await userService.createUser(req.body);

      res.status(201).json(user);
    } catch (err) {
      next(err);
    }
  },

  getAllUsers: async (req, res, next) => {
    try {
      const users = await userService.getAllUsers();

      res.status(201).json(users);
    } catch (err) {
      next(err);
    }
  },

  getUserById: (req, res, next) => {
    try {
      res.json(req.locals.user);
    } catch (err) {
      next(err);
    }
  },

  updateUserById: async (req, res, next) => {
    try {
      const userId = req.params.user_id;

      const user = await userService.updateUser(userId, req.body);

      res.status(200).json(user);
    } catch (err) {
      next(err);
    }
  },

  deleteUserById: async (req, res, next) => {
    try {
      const userId = req.params.user_id;

      await userService.deleteUser(userId);

      res.status(200).json({ id: userId });
    } catch (err) {
      next(err);
    }
  },
};
