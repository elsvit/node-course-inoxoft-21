module.exports = {
  organizationController: require('./organization.controller'),
  userController: require('./user.controller'),
};
