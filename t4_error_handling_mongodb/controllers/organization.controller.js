const { organizationService } = require('../services');

module.exports = {
  createOrganization: async (req, res, next) => {
    try {
      const organization = await organizationService.createOrganization(req.body);

      res.status(201).json(organization);
    } catch (err) {
      next(err);
    }
  },

  getAllOrganizations: async (req, res, next) => {
    try {
      const organizations = await organizationService.getAllOrganizations();

      res.status(201).json(organizations);
    } catch (err) {
      next(err);
    }
  },

  getOrganizationById: (req, res, next) => {
    try {
      res.json(req.locals.organization);
    } catch (err) {
      next(err);
    }
  },

  updateOrganizationById: async (req, res, next) => {
    try {
      const organizationId = req.params.organization_id;

      const organization = await organizationService.updateOrganization(organizationId, req.body);

      res.status(200).json(organization);
    } catch (err) {
      next(err);
    }
  },

  deleteOrganizationById: async (req, res, next) => {
    try {
      const organizationId = req.params.organization_id;

      await organizationService.deleteOrganization(organizationId);

      res.status(200).json({ id: organizationId });
    } catch (err) {
      next(err);
    }
  },
};
