module.exports = {
  SOMETHING_WRONG: 'Something wrong',
  UNAUTHORIZED: 'Unauthorized',
  EMAIL_EXISTS: 'Email already exists',
  ORGANIZATION_EXISTS: 'Organization already exists',
  NOT_FOUND: 'Not found',
  CONFLICT: 'Conflict',
};
