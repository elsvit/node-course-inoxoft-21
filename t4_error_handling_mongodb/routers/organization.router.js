const router = require('express').Router();

const { organizationController } = require('../controllers');
const { checkOrganizationIdExists, checkOrganizationUniqueName } = require('../middlewares/organization.middleware');

router.post('/', checkOrganizationUniqueName, organizationController.createOrganization);

router.get('/', organizationController.getAllOrganizations);

router.get('/:organization_id', checkOrganizationIdExists, organizationController.getOrganizationById);

router.put('/:organization_id', checkOrganizationIdExists, organizationController.updateOrganizationById);

router.delete('/:organization_id', checkOrganizationIdExists, organizationController.deleteOrganizationById);

module.exports = router;
