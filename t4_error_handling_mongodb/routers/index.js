module.exports = {
  organizationRouter: require('./organization.router'),
  userRouter: require('./user.router'),
};
