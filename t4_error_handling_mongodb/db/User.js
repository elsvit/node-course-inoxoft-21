const { Schema, model } = require('mongoose');
const userRole = require('../configs/userRole.enum');

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true
  },
  role: {
    type: String,
    default: userRole.USER,
    enum: Object.values(userRole)
  }
}, { timestamps: true });

module.exports = model('user', userSchema);
