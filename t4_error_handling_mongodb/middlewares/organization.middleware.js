const { organizationService } = require('../services');
const { ErrorHandler, errorMessage, errorStatus } = require('../errors');

module.exports = {
  checkOrganizationUniqueName: async (req, res, next) => {
    try {
      const { name } = req.body;

      const organizationByName = await organizationService.getOrganizationByName(name.trim());

      if (organizationByName) {
        throw new ErrorHandler(errorStatus.CONFLICT, errorMessage.ORGANIZATION_EXISTS);
      }

      next();
    } catch (err) {
      next(err);
    }
  },

  checkOrganizationIdExists: async (req, res, next) => {
    try {
      const organization = await organizationService.getOrganizationById(req.params.organization_id);

      if (!organization) {
        throw new ErrorHandler(errorStatus.NOT_FOUND, errorMessage.NOT_FOUND);
      }

      req.locals = { organization };

      next();
    } catch (err) {
      next(err);
    }
  }
};
