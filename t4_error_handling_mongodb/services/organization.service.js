const { Organization } = require('../db');

const createOrganization = async (newOrganization) => {
  const organization = await Organization.create(newOrganization);
  return organization;
};

const getAllOrganizations = async () => {
  const allOrganizations = await Organization.find({});
  return allOrganizations;
};

const getOrganizationById = async (organizationId) => {
  const organization = await Organization.findOne({ id: organizationId });
  return organization;
};

const getOrganizationByName = async (organizationName) => {
  const organization = await Organization.findOne({ name: organizationName });
  return organization;
};

const updateOrganization = async (organizationId, organizationData) => {
  const organization = await Organization.findByIdAndUpdate(organizationId, organizationData);
  return organization;
};

const deleteOrganization = async (organizationId) => {
  const id = await Organization.findByIdAndDelete(organizationId);
  return id;
};

module.exports = {
  createOrganization,
  getAllOrganizations,
  getOrganizationById,
  getOrganizationByName,
  updateOrganization,
  deleteOrganization
};
