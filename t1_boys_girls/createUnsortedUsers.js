const fs = require('fs');
const path = require('path');
const util = require('util');

// const fsReadPromise = util.promisify(fs.readFile);
// const fsRenamePromise = util.promisify(fs.rename);
const fsWritePromise = util.promisify(fs.writeFile);
// const fsRmdirPromise = util.promisify(fs.rmdir);
// const fsMkdirPromise = util.promisify(fs.mkdir);
const fsReaddirPromise = util.promisify(fs.readdir);
const fsUnlinkPromise = util.promisify(fs.unlink);

const MALE = 'male';
const MALE_PATH = 'boys';
const FEMALE = 'female';
const FEMALE_PATH = 'girls';
const girlNames = [
  'Jane',
  'Ana',
  'Ganna',
  'Inna',
  'Marta',
  'Sophia',
  'Mila',
  'Oksana',
  'Liudmyla'
];
const boyNames = [
  'Ioan',
  'Ivan',
  'Vasyl',
  'Max',
  'Sergii',
  'Mykola',
  'Nazar',
  'Spartak',
  'Aganemnon'
];

function cleardDir(folderPath) {
  fsReaddirPromise(folderPath)
    .then((dir) => {
      dir.forEach((file) => {
        fsUnlinkPromise(path.join(folderPath, file)).catch((err) => console.log('UNLINK ERROR', err));
      });
    })
    .catch((err) => console.log(err.message || 'ERROR'));
}
cleardDir(MALE_PATH);
cleardDir(FEMALE_PATH);

const writeToRndFolder = (person, name) => {
  const rnd = Math.random();
  const isGirlsFolder = rnd < 0.5;
  const rndFolder = isGirlsFolder ? FEMALE_PATH : MALE_PATH;
  const filePath = path.join(rndFolder, name);
  fsWritePromise(filePath, person)
    .catch((err) => console.log('ERROR', err));
};

const generatePerson = (gender, name) => (
  JSON.stringify({
    gender,
    name,
    age: Math.round(Math.random() * 100)
  })
);

const createPersons = (gender, names) => {
  names.forEach((name) => {
    const person = generatePerson(gender, name);
    console.log('person', person);
    writeToRndFolder(person, name);
  });
};

createPersons(FEMALE, girlNames);
createPersons(MALE, boyNames);
