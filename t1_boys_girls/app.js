const fs = require('fs');
const path = require('path');
const util = require('util');

const fsReadPromise = util.promisify(fs.readFile);
const fsRenamePromise = util.promisify(fs.rename);
const fsReaddirPromise = util.promisify(fs.readdir);

const MALE = {
  gender: 'male',
  path: 'boys'
};
const FEMALE = {
  gender: 'female',
  path: 'girls'
};

const movePersonsToRightPlace = (folderPath) => {
  const getPath = (folder, file) => path.join(__dirname, folderPath, file);

  fsReaddirPromise(folderPath)
    .then((dir) => {
      dir.forEach((file) => {
        const filePath = getPath(folderPath, file);

        fsReadPromise(filePath)
          .then((res) => {
            const person = JSON.parse(res.toString());
            const isGirl = person.gender === FEMALE.gender;
            const rightPath = isGirl ? FEMALE.path : MALE.path;

            if (rightPath !== folderPath) {
              const newPath = getPath(rightPath, file);
              fsRenamePromise(filePath, newPath).catch((err) => console.log('ERROR fsRenamePromise', err));
            }
          })
          .catch((err) => console.log('ERROR fsReadPromise', err));
      });
    })
    .catch((err) => console.log('movePersonsToRightPlace ERROR', err));
};

movePersonsToRightPlace(FEMALE.path);
movePersonsToRightPlace(MALE.path);
