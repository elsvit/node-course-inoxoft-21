const { Schema, model } = require('mongoose');

const { dbTablesEnum, userRolesEnum, userStatusesEnum } = require('../configs');

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    trim: true,
    // select: false
  },
  user_status: {
    type: String,
    default: userStatusesEnum.EMAIL_VERIFICATION,
    enum: Object.values(userStatusesEnum)
  },
  role: {
    type: String,
    default: userRolesEnum.USER,
    enum: Object.values(userRolesEnum)
  }
}, { timestamps: true });

module.exports = model(dbTablesEnum.USER, userSchema);
