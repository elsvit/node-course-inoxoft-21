const { Schema, model } = require('mongoose');

const { dbTablesEnum } = require('../configs');

const itemSchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    unique: true
  },
  [dbTablesEnum.USER]: {
    type: Schema.Types.ObjectId,
    ref: dbTablesEnum.USER,
    required: true
  },
  description: {
    type: String,
    required: false,
    trim: true,
  },
}, { timestamps: true });

module.exports = model(dbTablesEnum.ITEM, itemSchema);
