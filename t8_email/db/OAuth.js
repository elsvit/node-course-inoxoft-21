const { Schema, model } = require('mongoose');

const { dbTablesEnum, constants: { TOKEN_TYPE } } = require('../configs');

const OAuthSchema = new Schema({
  [TOKEN_TYPE.ACCESS]: {
    type: String,
    required: true
  },
  [TOKEN_TYPE.REFRESH]: {
    type: String,
    required: true
  },
  [dbTablesEnum.USER]: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: dbTablesEnum.USER
  },
}, { timestamps: true });

module.exports = model(dbTablesEnum.OAUTH, OAuthSchema);
