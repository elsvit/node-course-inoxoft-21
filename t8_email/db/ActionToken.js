const { Schema, model } = require('mongoose');
const { dbTablesEnum, actionEnum } = require('../configs');

const ActionTokenSchema = new Schema({
  token: {
    type: String,
    required: true
  },
  [dbTablesEnum.USER]: {
    type: Schema.Types.ObjectId,
    ref: dbTablesEnum.USER,
    required: true
  },
  action: {
    enum: Object.values(actionEnum),
    required: true,
    type: String
  }
});

module.exports = model(dbTablesEnum.ACTION_TOKEN, ActionTokenSchema);
