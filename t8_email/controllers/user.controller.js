const { config, emailActionsEnum } = require('../configs');
const {
  userService, passwordService, emailService, jwtService
} = require('../services');
const { userUtil } = require('../utils');

module.exports = {
  createUser: async (req, res, next) => {
    try {
      const { password } = req.body;

      const hashPassword = await passwordService.hash(password);

      const user = await userService.createUser({ ...req.body, password: hashPassword });

      const confirmEmailToken = jwtService.generateToken(config.CONFIRM_EMAIL_TOKEN_SECRET);
      const confirmUrl = `${config.HOST}:${config.PORT}/confirm-email/${confirmEmailToken}`;

      await emailService.sendMail(
        // config.EMAIL_BROADCAST,
        user.email,
        config.EMAIL_BROADCAST,
        emailActionsEnum.CONFIRM_EMAIL,
        { userName: user.name, confirmUrl }
      );

      const normalizedUser = userUtil.normalizeUser(user);

      res.status(201).json(normalizedUser);
    } catch (err) {
      next(err);
    }
  },

  getAllUsers: async (req, res, next) => {
    try {
      const users = await userService.getAllUsers();

      res.status(201).json(users);
    } catch (err) {
      next(err);
    }
  },

  getUserById: (req, res, next) => {
    try {
      const normalizedUser = userUtil.normalizeUser(req.locals.user);

      res.json(normalizedUser);
    } catch (err) {
      next(err);
    }
  },

  updateUserById: async (req, res, next) => { // TODO check
    try {
      const userId = req.params.user_id;

      const { password, ...rest } = req.body;

      let hashPassword = req.locals.user.password;

      if (password) {
        hashPassword = await passwordService.hash(password);
      }

      const user = await userService.updateUser(userId, { ...rest, password: hashPassword });

      res.json(user);
    } catch (err) {
      next(err);
    }
  },

  deleteUserById: async (req, res, next) => {
    try {
      const userId = req.params.user_id;

      await userService.deleteUser(userId);

      res.json({ id: userId });
    } catch (err) {
      next(err);
    }
  },
};
