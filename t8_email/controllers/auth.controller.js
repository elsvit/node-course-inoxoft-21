const { OAuth, ActionToken, User } = require('../db');
const {
  constants, statusCodesEnum, dbTablesEnum, actionEnum, userStatusesEnum
} = require('../configs');
const { passwordService, oauthService, jwtService } = require('../services');
const { userUtil } = require('../utils');

module.exports = {
  activateUser: async (req, res, next) => {
    try {
      const { locals: { user } } = req;

      await ActionToken.findOneAndDelete({ [dbTablesEnum.USER]: user._id, action: actionEnum.CONFIRM_EMAIL });

      await User.findByIdAndUpdate(user._id, { ...user, user_status: userStatusesEnum.ACTIVE });

      const normalizedUser = userUtil.normalizeUser(user);

      res.json(normalizedUser);
    } catch (err) {
      next(err);
    }
  },

  loginUser: async (req, res, next) => {
    try {
      const { locals: { user }, body: { password } } = req;

      await passwordService.compare(password, user.password);

      const tokenPair = jwtService.generateTokenPair();

      await oauthService.create({ ...tokenPair, user: user._id });

      const normalizedUser = userUtil.normalizeUser(user);

      res.json({
        ...tokenPair,
        user: normalizedUser
      });
    } catch (err) {
      next(err);
    }
  },

  logoutUser: async (req, res, next) => {
    try {
      const token = req.get(constants.AUTHORIZATION);

      await oauthService.deleteToken({ tokenType: constants.TOKEN_TYPE.ACCESS, token });

      res.status(statusCodesEnum.NO_CONTENT).json('Ok');
    } catch (err) {
      next(err);
    }
  },

  refreshToken: async (req, res, next) => {
    try {
      const token = req.get(constants.AUTHORIZATION);
      const { currentUser } = req;

      await oauthService.deleteToken({ tokenType: constants.TOKEN_TYPE.REFRESH, token });

      const tokenPair = jwtService.generateTokenPair();

      await OAuth.create({ ...tokenPair, user: currentUser._id });

      res.json({
        ...tokenPair,
        user: userUtil.normalizeUser(currentUser)
      });
    } catch (err) {
      next(err);
    }
  },
};
