const { CONFIRM_EMAIL } = require('../configs/emailActions.enum');

module.exports = {
  [CONFIRM_EMAIL]: {
    templateName: 'confirm-email',
    subject: 'Confirm signup email'
  },
};
