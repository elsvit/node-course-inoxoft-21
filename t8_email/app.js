const express = require('express');
const mongoose = require('mongoose');

require('dotenv').config();

const {
  authRouter,
  // itemRouter,
  userRouter
} = require('./routers');
const { apiRoutes, config } = require('./configs');

mongoose.connect(config.DB_CONNECT_URL);

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(`/${apiRoutes.AUTH}`, authRouter);

// app.use(`/${apiRoutes.ITEM}`, itemRouter);

app.use(`/${apiRoutes.USERS}`, userRouter);

app.use(_mainErrorHandler);

app.get('/ping', (req, res) => res.send('Pong'));

app.listen(config.PORT, () => {
  console.log('App listen 5000');
});

// eslint-disable-next-line no-unused-vars
function _mainErrorHandler(err, req, res, next) {
  res
    .status(err.status || 500)
    .json({ message: err.message });
}
