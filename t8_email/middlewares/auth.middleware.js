const { OAuth, ActionToken } = require('../db');
const {
  constants,
  statusCodesEnum,
  dbTablesEnum
} = require('../configs');
const ErrorHandler = require('../errors/ErrorHandler');
const { jwtService } = require('../services');
const { userValidator } = require('../validators');
const { errorMessage } = require('../errors');

module.exports = {
  checkAccessToken: async (req, res, next) => {
    try {
      const token = req.get(constants.AUTHORIZATION);

      if (!token) {
        throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.NO_TOKEN);
      }

      await jwtService.verifyToken(token);

      const tokenFromDB = await OAuth.findOne({ access_token: token }).populate(dbTablesEnum.USER);

      if (!tokenFromDB) {
        throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.INVALID_TOKEN);
      }

      req.currentUser = tokenFromDB.user;

      next();
    } catch (err) {
      next(err);
    }
  },

  checkRefreshToken: async (req, res, next) => {
    try {
      const token = req.get(constants.AUTHORIZATION);

      if (!token) {
        throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.NO_TOKEN);
      }

      await jwtService.verifyToken(token, constants.TOKEN_TYPE.REFRESH);

      const tokenFromDB = await OAuth.findOne({ refresh_token: token }).populate(dbTablesEnum.USER);

      if (!tokenFromDB) {
        throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.INVALID_TOKEN);
      }

      req.currentUser = tokenFromDB.user;

      next();
    } catch (err) {
      next(err);
    }
  },

  checkActionToken: (action) => async (req, res, next) => {
    try {
      const { token } = req.params;

      if (!token) {
        throw new ErrorHandler(statusCodesEnum.NOT_FOUND, errorMessage.NO_TOKEN);
      }

      await jwtService.verifyToken(token, action);

      const tokenFromDB = await ActionToken.findOne({ action_token: token, action });

      if (!tokenFromDB) {
        throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.INVALID_TOKEN);
      }
      console.log('checkActionToken tokenFromDB', tokenFromDB, 'user', tokenFromDB.user);
      req.locals.user = tokenFromDB.user;

      next();
    } catch (err) {
      next(err);
    }
  },

  validateLogin: (req, res, next) => {
    try {
      const {
        // value,
        error,
      } = userValidator.loginUser.validate(req.body);

      if (error) {
        throw new ErrorHandler(statusCodesEnum.BAD_REQUEST, error.message || errorMessage.NOT_FOUND);
      }

      next();
    } catch (err) {
      throw new ErrorHandler(statusCodesEnum.BAD_REQUEST, errorMessage.NOT_FOUND);
    }
  },
};
