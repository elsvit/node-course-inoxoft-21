const { userService } = require('../services');
const { statusCodesEnum, userStatusesEnum } = require('../configs');
const { ErrorHandler, errorMessage } = require('../errors');
const { userValidator } = require('../validators');

module.exports = {
  validateSignup: (req, res, next) => {
    try {
      const {
        // value,
        error,
      } = userValidator.signupUser.validate(req.body);
      console.log('validateSignup', error);
      if (error) {
        throw new ErrorHandler(statusCodesEnum.CONFLICT, error.message);
      }

      next();
    } catch (err) {
      throw new ErrorHandler(statusCodesEnum.CONFLICT, err.message || errorMessage.CONFLICT);
    }
  },

  checkUserEmailUnique: async (req, res, next) => {
    try {
      const { email } = req.body;

      const userByEmail = await userService.getUserByEmail(email.trim());
      console.log('checkUserEmailUnique', userByEmail);
      if (userByEmail) {
        throw new ErrorHandler(statusCodesEnum.CONFLICT, errorMessage.EMAIL_EXISTS);
      }

      next();
    } catch (err) {
      next(err);
    }
  },

  checkUserEmailExist: async (req, res, next) => {
    try {
      const { email } = req.body;

      const user = await userService.getUserByEmailWithPassword(email.trim());

      if (!user) {
        throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.EMAIL_OR_PASSWORD_WRONG);
      }

      req.locals = { user };
      next();
    } catch (err) {
      next(err);
    }
  },

  checkUserIdExists: async (req, res, next) => {
    try {
      const user = await userService.getUserById(req.params.user_id);

      if (!user) {
        throw new ErrorHandler(statusCodesEnum.NOT_FOUND, errorMessage.NOT_FOUND);
      }

      req.locals = { user };
      console.log('checkUserIdExists user', user);

      next();
    } catch (err) {
      next(err);
    }
  },

  checkUserStatus: (req, res, next) => {
    try {
      const { user_status } = req.locals.user;

      if (user_status === userStatusesEnum.ACTIVE) {
        return next();
      }

      throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.EMAIL_NEEDS_CONFIRMATION);
    } catch (err) {
      next(err);
    }
  },

  checkUserUpdateRights: (req, res, next) => {
    try {
      const { user } = req.locals;
      console.log('req.params.user_id', req.params.user_id);
      console.log('checkUserUpdateRights user', user, 'req.body', req.body);
      return next();

      // throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.UNAUTHORIZED);
    } catch (err) {
      next(err);
    }
  },

  checkUserRole: (roleArr = []) => (req, res, next) => {
    try {
      const { role } = req.locals.user;

      if (!roleArr.length) {
        return next();
      }

      if (!roleArr.includes(role)) {
        throw new ErrorHandler(statusCodesEnum.FORBIDDEN, errorMessage.FORBIDDEN);
      }

      next();
    } catch (err) {
      next(err);
    }
  },
};
