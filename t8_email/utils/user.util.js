const normalizeUser = (user) => {
  const filterToRemove = [
    'password',
    '__v'
  ];

  const userObj = user.toObject();

  filterToRemove.forEach((filed) => {
    delete userObj[filed];
  });

  return userObj;
};

module.exports = {
  normalizeUser
};
