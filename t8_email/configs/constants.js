module.exports = {
  CURRENT_YEAR: new Date().getFullYear(),
  PASSWORD_REGEXP: new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{3,50})/),
  EMAIL_REGEXP: new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$'),
  AUTHORIZATION: 'Authorization',
  TOKEN_TYPE: {
    ACCESS: 'access_token',
    REFRESH: 'refresh_token',
    CONFIRM_EMAIL: 'confirm-email',
  }
};
