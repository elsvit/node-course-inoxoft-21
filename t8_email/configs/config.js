module.exports = {
  PORT: 5000,
  HOST: process.env.HOST || 'localhost',
  DB_CONNECT_URL: process.env.DB_CONNECT_URL || 'mongodb://localhost:27017/inoxoft',

  ACCESS_TOKEN_SECRET: process.env.ACCESS_TOKEN_SECRET || 'defaultAccessWord',
  REFRESH_TOKEN_SECRET: process.env.REFRESH_TOKEN_SECRET || 'defaultRefreshWord',
  CONFIRM_EMAIL_TOKEN_SECRET: process.env.CONFIRM_EMAIL_TOKEN_SECRET || 'confirmEmailWord',

  EMAIL_BROADCAST: process.env.EMAIL_BROADCAST || 'test@gmail.com',
  EMAIL_BROADCAST_PASS: process.env.EMAIL_BROADCAST_PASS || '12345',
  FRONTED_URL: process.env.FRONTED_URL || 'https://inoxoft.com',
};
