const router = require('express').Router();

const { actionEnum } = require('../configs');
const { authController } = require('../controllers');
const { userMiddleware, authMiddleware } = require('../middlewares');

router.post(
  '/login',
  authMiddleware.validateLogin,
  userMiddleware.checkUserEmailExist,
  authController.loginUser,
);

router.post('/logout', authMiddleware.checkAccessToken, authController.logoutUser);

router.post('/refresh', authMiddleware.checkRefreshToken, authController.refreshToken);

router.post('/confirm-email/:token', authMiddleware.checkActionToken(actionEnum.CONFIRM_EMAIL), authController.activateUser);

module.exports = router;
