const router = require('express').Router();

const { itemController } = require('../controllers');
const { checkItemIdExists, checkItemUniqueName } = require('../middlewares/item.middleware');

router.get('/', itemController.getAllItems);

router.post('/', checkItemUniqueName, itemController.createItem);

router.use(
  '/:user_id',
  checkItemIdExists
);

router.get('/:item_id', itemController.getItemById);

router.put('/:item_id', itemController.updateItemById);

router.delete('/:item_id', itemController.deleteItemById);

module.exports = router;
