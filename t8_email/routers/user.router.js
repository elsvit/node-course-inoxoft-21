const router = require('express').Router();

const { userController } = require('../controllers');
const {
  checkUserIdExists,
  checkUserEmailUnique,
  checkUserStatus,
  checkUserUpdateRights,
  // checkUserRole,
  validateSignup
} = require('../middlewares/user.middleware');

router.get('/', userController.getAllUsers);

router.post(
  '/',
  validateSignup,
  checkUserEmailUnique,
  userController.createUser,
);

router.use(
  '/:user_id',
  // userMiddlewares.getUserByDynamicParam('user_id', 'params', '_id'),
  checkUserIdExists
);

router.get(
  '/:user_id',
  // checkUserRole,
  userController.getUserById,
);

router.put(
  '/:user_id',
  checkUserUpdateRights,
  checkUserStatus,
  userController.updateUserById,
);

router.delete(
  '/:user_id',
  userController.deleteUserById,
);

module.exports = router;
