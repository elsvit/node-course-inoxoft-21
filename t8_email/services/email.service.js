const EmailTemplates = require('email-templates');
const nodemailer = require('nodemailer');
const path = require('path');

const emailTemplates = require('../email-templates');
const { errorMessage } = require('../errors');

const { config, statusCodesEnum } = require('../configs');
const ErrorHandler = require('../errors/ErrorHandler');

const templateParser = new EmailTemplates({
  views: {
    root: path.join(process.cwd(), 'email-templates')
  }
});

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: config.EMAIL_BROADCAST,
    pass: config.EMAIL_BROADCAST_PASS
  }
});

const sendMail = async (userMail, emailAction, context = {}) => {
  const templateToSend = emailTemplates[emailAction];
  const extendedContext = { ...context, frontendURL: config.FRONTED_URL };

  if (!templateToSend) {
    throw new ErrorHandler(statusCodesEnum.SERVER_ERROR, errorMessage.WRONG_EMAIL_TEMPLATE_NAME);
  }

  const { templateName, subject } = templateToSend;

  const html = await templateParser.render(templateName, extendedContext);

  return transporter.sendMail({
    from: 'No reply',
    to: userMail,
    subject,
    html
  });
};

module.exports = {
  sendMail
};

// let counter = 0;
//
// function send(adsa) {
//     try {
//         // SEND EMAIL CODE
//     } catch (e) {
//         ++counter;
//
//         if (counter > 3) {
//             throw new ErrorHandler(400, 'dasdas');
//         }
//         send(adsa);
//     }
// }
