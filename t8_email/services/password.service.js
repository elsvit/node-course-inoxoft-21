const bcrypt = require('bcrypt');

const { ErrorHandler, errorStatus, errorMessage } = require('../errors');

module.exports = {
  hash: (password) => bcrypt.hash(password, 10),
  compare: async (password, hashPassword) => {
    const isPasswordMatched = await bcrypt.compare(password, hashPassword);

    if (!isPasswordMatched) {
      throw new ErrorHandler(errorStatus.UNAUTHORIZED, errorMessage.EMAIL_OR_PASSWORD_WRONG);
    }
  }
};
