module.exports = {
  emailService: require('./email.service'),
  itemService: require('./item.service'),
  jwtService: require('./jwt.service'),
  oauthService: require('./oauth.service'),
  passwordService: require('./password.service'),
  userService: require('./user.service'),
};
