const jwt = require('jsonwebtoken');

const { statusCodesEnum, config, constants: { TOKEN_TYPE } } = require('../configs');
const { ErrorHandler, errorMessage } = require('../errors');

const ACCESS_TOKEN_EXPIRES_IN = '15m';
const REFRESH_TOKEN_EXPIRES_IN = '31d';
const DEFAULT_TOKEN_EXPIRES_IN = '7d';

module.exports = {
  generateToken: (secret, expiresIn = DEFAULT_TOKEN_EXPIRES_IN) => jwt.sign({}, secret, { expiresIn }),

  generateTokenPair: () => {
    const access_token = this.generateToken(config.ACCESS_TOKEN_SECRET, ACCESS_TOKEN_EXPIRES_IN);
    const refresh_token = this.generateToken(config.REFRESH_TOKEN_SECRET, REFRESH_TOKEN_EXPIRES_IN);
    // const access_token = jwt.sign({}, config.ACCESS_TOKEN_SECRET, { expiresIn: ACCESS_TOKEN_EXPIRES_IN });
    // const refresh_token = jwt.sign({}, config.REFRESH_TOKEN_SECRET, { expiresIn: REFRESH_TOKEN_EXPIRES_IN });
    console.log('generateTokenPair access_token', access_token, 'refresh_token', refresh_token);

    return {
      access_token,
      refresh_token
    };
  },

  verifyToken: (token, tokenType = TOKEN_TYPE.ACCESS) => {
    try {
      const secret = tokenType === TOKEN_TYPE.ACCESS ? config.ACCESS_TOKEN_SECRET : config.REFRESH_TOKEN_SECRET;

      jwt.verify(token, secret);
    } catch (err) {
      throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, err.message || errorMessage.INVALID_TOKEN);
    }
  }
};
