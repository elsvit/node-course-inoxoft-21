const { Item } = require('../db');

const createItem = async (newItem) => {
  const item = await Item.create(newItem);
  return item;
};

const getAllItems = async () => {
  const allItems = await Item.find({});
  return allItems;
};

const getItemById = async (itemId) => {
  const item = await Item.findOne({ id: itemId });
  return item;
};

const getItemByName = async (itemName) => {
  const item = await Item.findOne({ name: itemName });
  return item;
};

const updateItem = async (itemId, itemData) => {
  const item = await Item.findByIdAndUpdate(itemId, itemData);
  return item;
};

const deleteItem = async (itemId) => {
  const id = await Item.findByIdAndDelete(itemId);
  return id;
};

module.exports = {
  createItem,
  getAllItems,
  getItemById,
  getItemByName,
  updateItem,
  deleteItem
};
