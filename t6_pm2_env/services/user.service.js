const { User } = require('../db');

const createUser = async (newUser) => {
  const user = await User.create(newUser);
  return user;
};

const getAllUsers = async () => {
  const allUsers = await User.find({});
  return allUsers;
};

const getUserById = async (userId) => {
  const user = await User.findOne({ id: userId });
  return user;
};

const getUserByEmailWithPassword = async (userEmail) => {
  const user = await User.findOne({ email: userEmail }).select('+password');
  return user;
};

const updateUser = async (userId, userData) => {
  const user = await User.findByIdAndUpdate(userId, userData);
  return user;
};

const deleteUser = async (userId) => {
  const id = await User.findByIdAndDelete(userId);
  return id;
};

module.exports = {
  createUser,
  getAllUsers,
  getUserById,
  getUserByEmailWithPassword,
  updateUser,
  deleteUser
};
