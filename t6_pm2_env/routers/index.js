module.exports = {
  authRouter: require('./auth.router'),
  organizationRouter: require('./organization.router'),
  userRouter: require('./user.router'),
};
