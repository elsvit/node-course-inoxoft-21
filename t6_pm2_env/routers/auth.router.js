const router = require('express').Router();

const { authController } = require('../controllers');
const { checkUserEmailExist } = require('../middlewares/user.middleware');

router.post('/login', checkUserEmailExist, authController.loginUser);

module.exports = router;
