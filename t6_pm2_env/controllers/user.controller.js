const { userService, passwordService } = require('../services');
const { userUtil } = require('../utils');

module.exports = {
  createUser: async (req, res, next) => {
    try {
      const { password } = req.body;

      const hashPassword = await passwordService.hash(password);

      const user = await userService.createUser({ ...req.body, password: hashPassword });

      const normalizedUser = userUtil.normalizeUser(user);

      res.status(201).json(normalizedUser);
    } catch (err) {
      next(err);
    }
  },

  getAllUsers: async (req, res, next) => {
    try {
      const users = await userService.getAllUsers();

      res.status(201).json(users);
    } catch (err) {
      next(err);
    }
  },

  getUserById: (req, res, next) => {
    try {
      const normalizedUser = userUtil.normalizeUser(req.locals.user);

      res.json(normalizedUser);
    } catch (err) {
      next(err);
    }
  },

  updateUserById: async (req, res, next) => {
    try {
      const userId = req.params.user_id;

      const { password, ...rest } = req.body;

      let hashPassword = req.locals.user.password;

      if (password) {
        hashPassword = await passwordService.hash(password);
      }

      const user = await userService.updateUser(userId, { ...rest, password: hashPassword });

      res.status(200).json(user);
    } catch (err) {
      next(err);
    }
  },

  deleteUserById: async (req, res, next) => {
    try {
      const userId = req.params.user_id;

      await userService.deleteUser(userId);

      res.status(200).json({ id: userId });
    } catch (err) {
      next(err);
    }
  },
};
