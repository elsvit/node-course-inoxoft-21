module.exports = {
  organizationMiddleware: require('./organization.middleware'),
  userMiddleware: require('./user.middleware'),
};
