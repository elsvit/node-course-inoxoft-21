const { passwordService } = require('../services');
const { userUtil } = require('../utils');

module.exports = {
  loginUser: async (req, res, next) => {
    try {
      await passwordService.compare(req.body.password, req.locals.user.password);

      const normalizedUser = userUtil.normalizeUser(req.locals.user);

      res.status(200).json(normalizedUser);
    } catch (err) {
      next(err);
    }
  },
};
