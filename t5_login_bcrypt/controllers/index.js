module.exports = {
  authController: require('./auth.controller'),
  organizationController: require('./organization.controller'),
  userController: require('./user.controller'),
};
