const express = require('express');
const mongoose = require('mongoose');

const {
  authRouter,
  organizationRouter,
  userRouter
} = require('./routers');
const { PORT } = require('./configs/config');

mongoose.connect('mongodb://localhost:27017/inoxoft');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/auth', authRouter);

app.use('/organizations', organizationRouter);

app.use('/users', userRouter);

app.use(_mainErrorHandler);

app.listen(PORT, () => {
  console.log('App listen 5000');
});

// eslint-disable-next-line no-unused-vars
function _mainErrorHandler(err, req, res, next) {
  res
    .status(err.status || 500)
    .json({ message: err.message });
}
