module.exports = {
  organizationService: require('./organization.service'),
  passwordService: require('./password.service'),
  userService: require('./user.service'),
};
