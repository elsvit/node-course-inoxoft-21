module.exports = {
  authMiddleware: require('./auth.middleware'),
  itemMiddleware: require('./item.middleware'),
  userMiddleware: require('./user.middleware'),
};
