const { itemService } = require('../services');
const { ErrorHandler, errorMessage, errorStatus } = require('../errors');

module.exports = {
  checkItemUniqueName: async (req, res, next) => {
    try {
      const { name } = req.body;

      const itemByName = await itemService.getItemByName(name.trim());

      if (itemByName) {
        throw new ErrorHandler(errorStatus.CONFLICT, errorMessage.ORGANIZATION_EXISTS);
      }

      next();
    } catch (err) {
      next(err);
    }
  },

  checkItemIdExists: async (req, res, next) => {
    try {
      const item = await itemService.getItemById(req.params.item_id);

      if (!item) {
        throw new ErrorHandler(errorStatus.NOT_FOUND, errorMessage.NOT_FOUND);
      }

      req.locals = { item };

      next();
    } catch (err) {
      next(err);
    }
  }
};
