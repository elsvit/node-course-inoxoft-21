const { userService } = require('../services');
const { statusCodesEnum } = require('../configs');
const { ErrorHandler, errorMessage } = require('../errors');
const { userValidator } = require('../validators');

module.exports = {
  validateSignup: (req, res, next) => {
    try {
      const {
        // value,
        error,
      } = userValidator.signupUser.validate(req.body);

      if (error) {
        throw new ErrorHandler(statusCodesEnum.CONFLICT, error.message);
      }

      next();
    } catch (err) {
      throw new ErrorHandler(statusCodesEnum.CONFLICT, err.message || errorMessage.CONFLICT);
    }
  },

  checkUserEmailUnique: async (req, res, next) => {
    try {
      const { email } = req.body;

      const userByEmail = await userService.getUserByEmail(email.trim());
      if (userByEmail) {
        throw new ErrorHandler(statusCodesEnum.CONFLICT, errorMessage.EMAIL_EXISTS);
      }

      next();
    } catch (err) {
      next(err);
    }
  },

  checkUserEmailExist: async (req, res, next) => {
    try {
      const { email } = req.body;

      const user = await userService.getUserByEmailWithPassword(email.trim());

      if (!user) {
        throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, errorMessage.EMAIL_OR_PASSWORD_WRONG);
      }

      req.locals = { user };
      next();
    } catch (err) {
      next(err);
    }
  },

  checkUserIdExists: async (req, res, next) => {
    try {
      const user = await userService.getUserById(req.params.user_id);

      if (!user) {
        throw new ErrorHandler(statusCodesEnum.NOT_FOUND, errorMessage.NOT_FOUND);
      }

      req.locals = { user };

      next();
    } catch (err) {
      next(err);
    }
  },

  checkUserRole: (roleArr = []) => (req, res, next) => {
    try {
      const { role } = req.user;

      if (!roleArr.length) {
        return next();
      }

      if (!roleArr.includes(role)) {
        throw new ErrorHandler(statusCodesEnum.FORBIDDEN, errorMessage.FORBIDDEN);
      }

      next();
    } catch (err) {
      next(err);
    }
  },
};
