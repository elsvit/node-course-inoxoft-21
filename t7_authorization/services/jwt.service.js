const jwt = require('jsonwebtoken');

const { statusCodesEnum, config, constants: { TOKEN_TYPE } } = require('../configs');
const { ErrorHandler, errorMessage } = require('../errors');

const ACCESS_TOKEN_EXPIRES_IN = '15m';
const REFRESH_TOKEN_EXPIRES_IN = '31d';

module.exports = {
  generateTokenPair: () => {
    const access_token = jwt.sign({}, config.ACCESS_TOKEN_SECRET, { expiresIn: ACCESS_TOKEN_EXPIRES_IN });
    const refresh_token = jwt.sign({}, config.REFRESH_TOKEN_SECRET, { expiresIn: REFRESH_TOKEN_EXPIRES_IN });

    return {
      access_token,
      refresh_token
    };
  },

  verifyToken: (token, tokenType = TOKEN_TYPE.ACCESS) => {
    try {
      const secret = tokenType === TOKEN_TYPE.ACCESS ? config.ACCESS_TOKEN_SECRET : config.REFRESH_TOKEN_SECRET;

      jwt.verify(token, secret);
    } catch (err) {
      throw new ErrorHandler(statusCodesEnum.UNAUTHORIZED, err.message || errorMessage.INVALID_TOKEN);
    }
  }
};
