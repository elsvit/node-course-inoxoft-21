const { constants: { TOKEN_TYPE } } = require('../configs');
const { OAuth } = require('../db');

const create = async (props) => {
  const res = await OAuth.create(props);
  return res;
};

const update = async (props) => {
  const res = await OAuth.updateOne(props);
  return res;
};

const deleteToken = async ({ tokenType = TOKEN_TYPE.ACCESS, token }) => {
  const res = await OAuth.deleteOne({ [tokenType]: token });
  return res;
};

module.exports = {
  create,
  update,
  deleteToken,
};
