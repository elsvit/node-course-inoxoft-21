const Joi = require('joi');

const { EMAIL_REGEXP, PASSWORD_REGEXP } = require('../configs/constants');
const { ADMIN, USER } = require('../configs/userRoles.enum');

const signupUser = Joi.object({
  name: Joi
    .string()
    .trim()
    .required(),
  email: Joi
    .string()
    .trim()
    .email()
    // .regex(EMAIL_REGEXP)
    .required(),
  password: Joi
    .string()
    .trim()
    .min(3)
    .max(128)
    .regex(PASSWORD_REGEXP)
    .required(),
  role: Joi
    .string()
    // .allow(...userRolesEnum)
    .allow(ADMIN, USER)
});

const loginUser = Joi.object({
  email: Joi
    .string()
    .trim()
    // .email()
    .regex(EMAIL_REGEXP)
    .required(),
  password: Joi
    .string()
    .trim()
    // .min(8)
    // .max(128)
    .regex(PASSWORD_REGEXP)
    .required(),
});

const getUserById = Joi.object({
  userId: Joi
    .string()
    .trim()
    .min(24)
    .max(24)
    .required()
});

const updateUser = Joi.object({
  email: Joi
    .string()
    .trim()
    // .email(),
    .regex(EMAIL_REGEXP),
  password: Joi
    .string()
    .trim()
    // .min(8)
    // .max(30)
    .regex(PASSWORD_REGEXP)
});

module.exports = {
  signupUser,
  loginUser,
  getUserById,
  updateUser
};
