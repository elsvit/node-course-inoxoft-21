const router = require('express').Router();

const { authController } = require('../controllers');
const { userMiddleware, authMiddleware } = require('../middlewares');

router.post(
  '/login',
  authMiddleware.validateLogin,
  userMiddleware.checkUserEmailExist,
  authController.loginUser,
);

router.post('/logout', authMiddleware.checkAccessToken, authController.logoutUser);

router.post('/refresh', authMiddleware.checkRefreshToken, authController.refreshToken);

module.exports = router;
