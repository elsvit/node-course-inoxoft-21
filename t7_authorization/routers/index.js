module.exports = {
  authRouter: require('./auth.router'),
  itemRouter: require('./item.router'),
  userRouter: require('./user.router'),
};
