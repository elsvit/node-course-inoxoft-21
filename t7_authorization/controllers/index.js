module.exports = {
  authController: require('./auth.controller'),
  itemController: require('./item.controller'),
  userController: require('./user.controller'),
};
