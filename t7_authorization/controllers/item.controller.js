const { itemService } = require('../services');

module.exports = {
  createItem: async (req, res, next) => {
    try {
      const item = await itemService.createItem(req.body);

      res.status(201).json(item);
    } catch (err) {
      next(err);
    }
  },

  getAllItems: async (req, res, next) => {
    try {
      const items = await itemService.getAllItems();

      res.status(201).json(items);
    } catch (err) {
      next(err);
    }
  },

  getItemById: (req, res, next) => {
    try {
      res.json(req.locals.item);
    } catch (err) {
      next(err);
    }
  },

  updateItemById: async (req, res, next) => {
    try {
      const itemId = req.params.item_id;

      const item = await itemService.updateItem(itemId, req.body);

      res.json(item);
    } catch (err) {
      next(err);
    }
  },

  deleteItemById: async (req, res, next) => {
    try {
      const itemId = req.params.item_id;

      await itemService.deleteItem(itemId);

      res.json({ id: itemId });
    } catch (err) {
      next(err);
    }
  },
};
