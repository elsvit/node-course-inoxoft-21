module.exports = {
  SOMETHING_WRONG: 'Something wrong',

  CONFLICT: 'Conflict',
  EMAIL_EXISTS: 'Email already exists',
  EMAIL_OR_PASSWORD_WRONG: 'Email or password is wrong',
  FORBIDDEN: 'Forbidden',
  INVALID_TOKEN: 'Invalid token',
  NOT_FOUND: 'Not found',
  ORGANIZATION_EXISTS: 'Item already exists',
  UNAUTHORIZED: 'Unauthorized',
  VALIDATION_ERROR: 'Validation Error',
};
