const fs = require('fs');
const path = require('path');
const util = require('util');

const statusCodes = require('../configs/statusCodes.enam');

const fsReadPromise = util.promisify(fs.readFile);
const fsWritePromise = util.promisify(fs.writeFile);

const usersPath = path.join(__dirname, '..', 'dataBase', 'users.json');

const saveUser = (newUser) => fsReadPromise(usersPath)
  .then((res) => {
    const users = JSON.parse(res.toString()) || [];
    const isEmailExist = !!users.find((user) => user.name === newUser.name);

    if (isEmailExist) {
      return ({ isOk: false, status: statusCodes.CONFLICT, message: 'User already exist' });
    }

    const newUsers = JSON.stringify([
      ...users,
      newUser
    ]);

    return fsWritePromise(usersPath, newUsers)
      .then(() => ({ isOk: true, data: newUser }))
      .catch((err) => ({ isOk: false, status: statusCodes.BAD_REQUEST, message: err.message || 'Error create User.' }));
  })
  .catch((err) => ({ isOk: false, status: statusCodes.BAD_REQUEST, message: err.message || 'Error create User' }));

const getUsers = () => fsReadPromise(usersPath)
  .then((res) => {
    const users = JSON.parse(res.toString());
    return ({ isOk: true, data: users });
  })
  .catch((err) => ({ isOk: false, status: statusCodes.BAD_REQUEST, message: err.message || 'Error get Users' }));

const loginUser = (askedUser) => fsReadPromise(usersPath)
  .then((res) => {
    const users = JSON.parse(res.toString());
    const user = users.find((val) => (val.name === askedUser.name));

    if (!user) {
      return ({ isOk: false, status: statusCodes.NOT_FOUND, message: 'User not found' });
    } if (user.password !== askedUser.password) {
      return ({ isOk: false, status: statusCodes.CONFLICT, message: 'Password is wrong' });
    }

    return ({ isOk: true, data: user });
  })
  .catch((err) => ({ isOk: false, status: statusCodes.BAD_REQUEST, message: err.message || 'Error login User' }));

const getUser = (name) => fsReadPromise(usersPath)
  .then((res) => {
    const users = JSON.parse(res.toString());
    const user = users.find((val) => (val.name === name));

    if (!user) {
      return ({ isOk: false, status: statusCodes.NOT_FOUND, message: 'User not found' });
    }

    return ({ isOk: true, data: user });
  })
  .catch((err) => ({ isOk: false, status: statusCodes.BAD_REQUEST, message: err.message || 'Error get User' }));

module.exports = {
  saveUser,
  loginUser,
  getUsers,
  getUser,
};
