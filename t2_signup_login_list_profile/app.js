const express = require('express');
const expressHbs = require('express-handlebars');
const path = require('path');

const { PORT } = require('./configs/config');
const statusCodes = require('./configs/statusCodes.enam');
const routs = require('./configs/routs.enam');
const {
  getUsers, saveUser, loginUser, getUser
} = require('./services/users');

const app = express();
const staticPath = path.join(__dirname, 'static');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(staticPath));
app.set('view engine', '.hbs');
app.engine('.hbs', expressHbs({ defaultLayout: false }));
app.set('views', staticPath);

app.post('/auth/signup', (req, res) => {
  saveUser(req.body)
    .then((resOfSave) => {
      if (!resOfSave.isOk) {
        res.status(resOfSave.status).redirect(`/${routs.signup}?error=${resOfSave.message}`);
        return;
      }
      res.redirect(`/${routs.login}`);
    })
    .catch(() => {
      res.status(statusCodes.BAD_REQUEST).redirect(`/${routs.signup}?error=Error`);
    });
});

app.post('/auth/login', (req, res) => {
  loginUser(req.body)
    .then((resOfSave) => {
      if (!resOfSave.isOk) {
        res.status(resOfSave.status).redirect(`/${routs.login}?error=${resOfSave.message}`);
        return;
      }
      res.redirect(`/${routs.users}/${req.body.name}`);
    })
    .catch(() => {
      res.status(statusCodes.BAD_REQUEST).redirect(`/${routs.login}?error=Error`);
    });
});

// renders
app.get('/signup', (req, res) => {
  res.render('signup', { error: req.query.error });
});

app.get('/login', (req, res) => {
  res.render('login', { error: req.query.error });
});

app.get('/users/:name', (req, res) => {
  const { name } = req.params;
  if (!name) {
    res.render('user', { error: 'Unknown user' });
    return;
  }
  getUser(name)
    .then((response) => {
      res.render('user', response.data); // todo need check if !isOk
    })
    .catch(() => {
      res.render('user', { error: 'Get user error' });
    });
});

app.get('/users', (req, res) => {
  getUsers()
    .then((response) => {
      res.render('users', { users: response.data }); // todo need check if !isOk
    })
    .catch(() => {
      res.render('users', { error: 'Get users error' });
    });
});

app.listen(PORT, () => {
  console.log('App listen 5000');
});
